Programming Paradigms:

Programming Paradigms is a way of building the structure and elements of computer programs.Capabilities and styles of various programming languages are defined by their supported programming paradigms.Just as software engineering is defined by differing methodologies, so the programming languages are defined by differing paradigms.Programming paradims are distuinguished into different types named as:imperative, declarative, functional, object-oriented, logic and symbolic programming.Overviews of different paradigms:

1) Imperative Paradigm : The phrase "First Do This Then do That" describes the spirit of imperative paradigm.The main idea is the command which has a measurable effect on the program state.Execution of computational steps in an order is governed by control stuctures.The main languages under this paradigm are Fortran,Algol,Pascal,Basic C.

2) Functional Paradigm : Functional programming paradigm represents the process of computation as calculation of stateless mathematical functions and thus attempts to eliminate or minimize side effects.All computations are done by calling functions in this paradigm.Languages under this paradogm are Falcon,Haskell,Dylan,Agda.

3) Logic Paradigm : The logic paradigm is about having a program which specifies what the problem is, not how to solve it.
Logic programming languages � such as PROLOG and expert systems� consist of knowledge (facts) and other properties about the problem (rules).
Logic programs are goal driven. The focus is on the goal to be achieved rather than the steps required to be performed.

4) Object Oriented Paradigm : An object-oriented program is constructed with the outset in concepts, which are important in the problem domain of interest. In that way, all the necessary technicalities of programming come in second row.In It,Data as well as operations are encapsulated in objects.Computer programs are designed by making them out of objects that interact with one another.Significant object-oriented languages include C++, Objective-C, Smalltalk, Delphi, Java, C#, Perl, Python, Ruby and PHP.

5) Multi Paradigm : A multi-paradigm programming language is a programming language that supports more than one programming paradigm.The idea of a multiparadigm language is to provide a framework in which programmers can work in a variety of styles, freely intermixing constructs from different paradigms.The design goal of such languages is to allow programmers to use the best tool for a job, admitting that no one paradigm solves all problems in the easiest or most efficient way.Examples are C#,F#,Scala And Oz.


References!:
http://people.cs.aau.dk/~normark/prog3-03/html/notes/paradigms_themes-paradigm-overview-section.html
http://en.wikipedia.org/wiki/Programming_paradigm
http://progopedia.com/paradigm/functional/
http://www.hsc.csu.edu.au/sdd/options/para/3194/logic.htm
http://www.exforsys.com/tutorials/c-plus-plus/object-oriented-programming-paradigm.html